# Spring Boot API for Price Product Management Required By Inditex.

This API provides endpoints to manage price products. It offers functionality to retrieve all price products, find a specific product by ID, and filter price products based on certain criteria.

## Table of Contents

- [Endpoints](#endpoints)
- [Setup and Running](#setup-and-running)
- [Project Structure](#project-structure)
- [Testing](#testing)

## Endpoints

1. **Retrieve all price products**
   - **URL**: `/price-product`
   - **Method**: `GET`

2. **Find price product by ID**
   - **URL**: `/price-product/{id}`
   - **Method**: `GET`

3. **Filter price products**
   - **URL**: `/price-product-filter`
   - **Method**: `GET`
   - **Query Parameters**:
     - `productId`: Long (Required)
     - `brandId`: Integer (Required)
     - `applicationDate`: Date string in the format "yyyy-MM-dd" (Required)

## Setup and Running

### Requirements

- Java JDK 11 or later
- Maven

### Steps

1. Clone the repository.
2. Navigate to the project directory.
3. Start the application with the command:
   ```bash
   mvn spring-boot:run
   ```

The application will initialize an in-memory H2 database and populate it with data from `data.sql` upon start-up.

### H2 Console

To access the H2 database console:
- **URL**: `http://localhost:8080/h2-console`
- **JDBC URL**: `jdbc:h2:mem:testdb`
- **User Name**: `sa`
- **Password**: (leave empty)

Ensure that the H2 console settings in `application.properties` are properly configured to allow connections.

## Project Structure

The project follows the hexagonal architecture pattern. The main components and their purposes are:

- `adapters`: Contains controllers for handling HTTP requests and the persistence layer for database operations.
- `domains`: Contains the core domain entities of the application.
- `ports`: Defines the primary ports/interfaces for the application.
- `useCases`: Contains the application's main business logic.
- `utils`: Utility classes, including error handling.

```├── README.md
├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── inditexApi
│   │   │           ├── Application.java
│   │   │           ├── adapters
│   │   │           │   ├── controllers
│   │   │           │   │   ├── PriceProductController.java
│   │   │           │   │   └── dtos
│   │   │           │   │       └── PriceProductDto.java
│   │   │           │   └── persistence
│   │   │           │       ├── PriceProductPersistant.java
│   │   │           │       └── PriceProductQuery.java
│   │   │           ├── domains
│   │   │           │   ├── Brand.java
│   │   │           │   ├── Currency.java
│   │   │           │   └── PriceProduct.java
│   │   │           ├── ports
│   │   │           │   └── repository
│   │   │           │       └── PriceProductRepository.java
│   │   │           ├── useCases
│   │   │           │   └── PriceProductUseCase.java
│   │   │           └── utils
│   │   │               └── errorHandling
│   │   │                   ├── ErrorMessages.java
│   │   │                   ├── ErrorResponse.java
│   │   │                   ├── ResourceExceptionHandler.java
│   │   │                   └── customExceptions
│   │   │                       └── ResourceNotFoundException.java
│   │   └── resources
│   │       ├── application.properties
│   │       ├── data.sql
│   │       └── schema.sql
│   └── test
│       └── java
│           └── com
│               └── inditexApi
│                   ├── e2e
│                   │   ├── CustomAppSpringBootTest.java
│                   │   └── PriceProductE2ETest.java
│                   ├── useCase
│                   └── useCases
│                       └── PriceProductUseCaseTest.java
└── target
    ├── classes
    │   ├── application.properties
    │   ├── com
    │   │   └── inditexApi
    │   │       ├── Application.class
    │   │       ├── adapters
    │   │       │   ├── controllers
    │   │       │   │   ├── PriceProductController.class
    │   │       │   │   └── dtos
    │   │       │   │       └── PriceProductDto.class
    │   │       │   └── persistence
    │   │       │       ├── PriceProductPersistant.class
    │   │       │       └── PriceProductQuery.class
    │   │       ├── domains
    │   │       │   ├── Brand.class
    │   │       │   ├── Currency.class
    │   │       │   └── PriceProduct.class
    │   │       ├── ports
    │   │       │   └── repository
    │   │       │       └── PriceProductRepository.class
    │   │       ├── useCases
    │   │       │   └── PriceProductUseCase.class
    │   │       └── utils
    │   │           └── errorHandling
    │   │               ├── ErrorMessages.class
    │   │               ├── ErrorResponse.class
    │   │               ├── ResourceExceptionHandler.class
    │   │               └── customExceptions
    │   │                   └── ResourceNotFoundException.class
    │   ├── data.sql
    │   └── schema.sql
    └── test-classes
        └── com
            └── inditexApi
                ├── e2e
                │   ├── CustomAppSpringBootTest.class
                │   └── PriceProductE2ETest.class
                ├── useCase
                └── useCases
                    └── PriceProductUseCaseTest.class
```

For a detailed view of the project's directory structure, see the tree structure provided.

## Testing

Tests are categorized into `e2e` for end-to-end tests and `useCases` for business logic tests. To run all tests, use the command:

```bash
mvn test
```
