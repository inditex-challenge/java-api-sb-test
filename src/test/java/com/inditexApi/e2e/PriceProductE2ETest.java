package com.inditexApi.e2e;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.inditexApi.adapters.controllers.dtos.PriceProductDto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = com.inditexApi.Application.class)
public class PriceProductE2ETest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    // Happy Path
    @Test
    public void testGetPriceProductsByFilterDay14At10h() {
        String applicationDateStr = "2020-06-14T10:00:00";
        Long productId = 35455L;
        Integer brandId = 1;

        Integer expectedPriceList = 1;
        BigDecimal expectedPrice = new BigDecimal("35.50");

        this.commonAsserts(productId, brandId, applicationDateStr, expectedPriceList, expectedPrice);
    }

    @Test
    public void testGetPriceProductsByFilterDay14At16h() {
        String applicationDateStr = "2020-06-14T16:00:00";
        Long productId = 35455L;
        Integer brandId = 1;

        Integer expectedPriceList = 2;
        BigDecimal expectedPrice = new BigDecimal("25.45");

        this.commonAsserts(productId, brandId, applicationDateStr, expectedPriceList, expectedPrice);
    }

    @Test
    public void testGetPriceProductsByFilterDay14At21h() {
        String applicationDateStr = "2020-06-14T21:00:00";
        Long productId = 35455L;
        Integer brandId = 1;

        Integer expectedPriceList = 1;
        BigDecimal expectedPrice = new BigDecimal("35.50");

        this.commonAsserts(productId, brandId, applicationDateStr, expectedPriceList, expectedPrice);
    }

    @Test
    public void testGetPriceProductsByFilterDay15At10h() {
        String applicationDateStr = "2020-06-15T10:00:00";
        Long productId = 35455L;
        Integer brandId = 1;

        Integer expectedPriceList = 3;
        BigDecimal expectedPrice = new BigDecimal("30.50");

        this.commonAsserts(productId, brandId, applicationDateStr, expectedPriceList, expectedPrice);
    }

    @Test
    public void testGetPriceProductsByFilterDay16At21h() {
        String applicationDateStr = "2020-06-16T21:00:00";
        Long productId = 35455L;
        Integer brandId = 1;

        Integer expectedPriceList = 4;
        BigDecimal expectedPrice = new BigDecimal("38.95");

        this.commonAsserts(productId, brandId, applicationDateStr, expectedPriceList, expectedPrice);
    }

    private void commonAsserts(
        Long productId,
        Integer brandId,
        String applicationDateStr,
        Integer expectedPriceList,
        BigDecimal expectedPrice
    ) {
        ResponseEntity<PriceProductDto> response = restTemplate.exchange(
            "http://localhost:" + port + "/price-product-by-filters" +
            "?productId=" + productId +
            "&applicationDate=" + applicationDateStr +
            "&brandId=" + brandId,
            HttpMethod.GET,
            null,
            PriceProductDto.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getProductId()).isEqualTo(productId);
        assertThat(response.getBody().getBrandId()).isEqualTo(brandId);

        assertThat(response.getBody().getPriceList()).isEqualTo(expectedPriceList);
        assertThat(response.getBody().getPrice()).isEqualTo(expectedPrice);
        assertTrue(response.getBody().getStartDate().isBefore(LocalDateTime.parse(applicationDateStr)));
        assertTrue(response.getBody().getEndDate().isAfter(LocalDateTime.parse(applicationDateStr)));
    }

    // Sad Path
    @Test
    public void testGetPriceProductsWithMissingFilters() throws Exception {
        ResponseEntity<String> response = restTemplate.exchange(
            "http://localhost:" + port + "/price-product-by-filters",
            HttpMethod.GET,
            null,
            String.class
        );

        // Better approach should be adding Jackson package in POM and handle string as a Json Object.
        // I consider to demostrate the point is enough,

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response.getBody(), "{\"status\":400,\"message\":\"Missing request parameter 'productId' of type 'Long'\"}");
    }

    @Test
    public void testGetPriceProductsWithMissingApplicationDate() throws Exception {
        Long productId = 35455L;
        Integer brandId = 1;
        ResponseEntity<String> response = restTemplate.exchange(
            "http://localhost:" + port + "/price-product-by-filters"+
            "?productId=" + productId +
            "&brandId=" + brandId,
            HttpMethod.GET,
            null,
            String.class
        );

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response.getBody(), "{\"status\":400,\"message\":\"Missing request parameter 'applicationDate' of type 'String'\"}");

    }

}
