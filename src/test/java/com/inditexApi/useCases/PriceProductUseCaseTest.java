package com.inditexApi.useCases;

import com.inditexApi.domains.Brand;
import com.inditexApi.domains.Currency;
import com.inditexApi.domains.PriceProduct;
import com.inditexApi.ports.repository.PriceProductRepository;
import com.inditexApi.adapters.persistence.PriceProductPersistant;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PriceProductUseCaseTest {

    @Mock
    private PriceProductRepository priceProductPersistant;

    @InjectMocks
    private PriceProductUseCase priceProductUseCase;

    PriceProduct priceProductMock;
    Long productId;
    LocalDateTime applicationDate;
    Integer brandId;

    @BeforeAll
    void setUp() {
        productId = 35455L;
        applicationDate = LocalDateTime.parse("2020-06-14T16:00:00");
        brandId = 1;

        priceProductMock = new PriceProduct(
            1L,
            new Brand(brandId, "Zara", true),
            LocalDateTime.parse("2020-06-14T15:00:00"),
            LocalDateTime.parse("2020-06-14T18:30:00"),
            1,
            productId,
            1,
            new BigDecimal("25.45"),
            new Currency(1, "EUR", true)
        );
    } 

    @Test
    void testFindAll() {
        // Given
        List<PriceProduct> expected = new ArrayList<PriceProduct>();

        expected.add(priceProductMock);
        when(priceProductPersistant.findAll()).thenReturn(expected);

        // When
        List<PriceProduct> result = priceProductUseCase.findAll();

        // Then
        assertEquals(expected, result);
    }

    @Test
    void testFindById() {
        // Given
        Long id = 1L;
        when(priceProductPersistant.findById(id)).thenReturn(Optional.of(priceProductMock));

        // When
        Optional<PriceProduct> result = priceProductUseCase.findById(id);

        // Then
        assertTrue(result.isPresent());
        assertEquals(priceProductMock, result.get());
    }

    @Test
    void testFindByProductIdAndStartDateBetweenAndBrandId() {
        // Given

        when(priceProductPersistant.findCustomByProductIdAndBrandIdAndApplicationDate(productId, applicationDate, brandId))
            .thenReturn(priceProductMock);

        // When
        PriceProduct result = priceProductUseCase.findByProductIdAndStartDateBetweenAndBrandId(productId, applicationDate, brandId);

        // Then
        assertEquals(priceProductMock, result);
    }

    // Should return empty response;
    @Test
    void testFindByProductIdAndStartDateBetweenAndBrandIdWithWrongDate() {
        // Given
        LocalDateTime applicationDateFuture = LocalDateTime.parse("2025-12-12 10:00:00");
        PriceProduct priceProductMockB = new PriceProduct();

        when(priceProductPersistant.findCustomByProductIdAndBrandIdAndApplicationDate(productId, applicationDateFuture, brandId))
            .thenReturn(priceProductMockB);

        // When
        PriceProduct result = priceProductUseCase.findByProductIdAndStartDateBetweenAndBrandId(productId, applicationDateFuture, brandId);

        // Then
        assertEquals(priceProductMock, result);
    }
}
