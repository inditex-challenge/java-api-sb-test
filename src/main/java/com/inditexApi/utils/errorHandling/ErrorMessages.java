package com.inditexApi.utils.errorHandling;

/**
 *  Final class to host all the constants message related to ErrorHandler
 */
public final class ErrorMessages {
    
    public static final String SERVER_ERROR = "Internal Error. Try later.";
}
