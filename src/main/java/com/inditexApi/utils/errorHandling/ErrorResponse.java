package com.inditexApi.utils.errorHandling;

public class ErrorResponse {
    private int status;
    private String message;

    public ErrorResponse(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() { return this.message; }

    public Integer getStatus() { return this.status; } 


}
