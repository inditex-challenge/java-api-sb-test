package com.inditexApi.adapters.persistence;

public final class PriceProductQuery {
    private final static String COMMON_SELECT = 
        " SELECT * FROM PRICE_PRODUCT pp " +
        " WHERE pp.PRODUCT_ID = :productId " +
        " AND pp.BRAND_ID = :brandId ";

    private final static String DESAMBIGUATOR_INSTRUCTION = " ORDER BY pp.PRIORITY DESC LIMIT 1";

    public static final String GET_BY_PRODUCT_ID_AND_BRAND_ID_BETWEEN_DATES = 
        COMMON_SELECT +
        " AND (pp.START_DATE <= :applicationDate AND pp.END_DATE >= :applicationDate) " +
        DESAMBIGUATOR_INSTRUCTION;

}