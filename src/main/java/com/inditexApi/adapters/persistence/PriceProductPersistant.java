package com.inditexApi.adapters.persistence;

import com.inditexApi.domains.PriceProduct;
import com.inditexApi.ports.repository.PriceProductRepository;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceProductPersistant extends PriceProductRepository, JpaRepository<PriceProduct, Long> {
    /**
     *  JPA bean that perform the main query to get the price product required.
     * I choose a native query to show you the approach of sql knowledge, instead 
     * use the Spring Data JPA Query Derivation name strategy.
     */
    @Query(value = PriceProductQuery.GET_BY_PRODUCT_ID_AND_BRAND_ID_BETWEEN_DATES, nativeQuery = true)
    PriceProduct findCustomByProductIdAndBrandIdAndApplicationDate(
            @Param("productId") Long productId,
            @Param("applicationDate") LocalDateTime applicationDate,
            @Param("brandId") Integer brandId
    );
}
