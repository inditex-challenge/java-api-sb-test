package com.inditexApi.adapters.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.inditexApi.adapters.controllers.dtos.PriceProductDto;
import com.inditexApi.domains.PriceProduct;
import com.inditexApi.useCases.PriceProductUseCase;
import com.inditexApi.utils.errorHandling.customExceptions.ResourceNotFoundException;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
public class PriceProductController {
    
    private final PriceProductUseCase priceProductService;
    
    @Autowired
    public PriceProductController(PriceProductUseCase priceProductService) {
        this.priceProductService = priceProductService;
    }
    
    @GetMapping("/price-product/{id}")
    public ResponseEntity<PriceProduct> getPriceProductById(@PathVariable Long id) {
        return ResponseEntity.of(priceProductService.findById(id));
    }
    
    /**
     * This method contains the logic for the challenge
     * 
     * @param Long productId
     * @param String applicationDateStr
     * @param Integer brandId
     * @return PriceProductDto
     * @throws ResponseStatusException
     */
    @RequestMapping(value = "/price-product-by-filters", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
    public PriceProductDto getPriceProductsByFilter(
    @RequestParam(name = "productId", required = true) Long productId,
    @RequestParam(name = "applicationDate", required = true) String applicationDateStr,
    @RequestParam(name = "brandId", required = true) Integer brandId)
    throws ResponseStatusException {

        LocalDateTime applicationDate = LocalDateTime.parse(applicationDateStr);
        
        PriceProduct filteredPrices = priceProductService.findByProductIdAndStartDateBetweenAndBrandId(productId, applicationDate, brandId);

        if (filteredPrices == null || filteredPrices.isEmpty(filteredPrices)) throw new ResourceNotFoundException("Price not found");
        return PriceProductDto.getResponseForFilteredPriceProduct(filteredPrices);
    }
}
