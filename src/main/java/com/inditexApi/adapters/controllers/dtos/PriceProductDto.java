package com.inditexApi.adapters.controllers.dtos;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inditexApi.domains.Currency;
import com.inditexApi.domains.PriceProduct;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceProductDto {

    private String currencyName;

    private Long id;
    private Integer brandId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Integer priceList;
    private Long productId;
    private Integer priority;
    private BigDecimal price;
    private Currency curr;

    // Constructors

    public PriceProductDto() {
    }
    
    public PriceProductDto(
        Long id,
        Integer brandId,
        LocalDateTime startDate, 
        LocalDateTime endDate,
        Integer priceList,
        Long productId, 
        Integer priority,
        BigDecimal price,
        Currency curr
    ) {
        this.id = id;
        this.brandId = brandId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceList = priceList;
        this.productId = productId;
        this.priority = priority;
        this.price = price;
        this.curr = curr;
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public Integer getPriceList() {
        return priceList;
    }

    public Long getProductId() {
        return productId;
    }

    public Integer getPriority() {
        return priority;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Currency getCurr() {
        return curr;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    // Devuelva como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar.
    public static PriceProductDto getResponseForFilteredPriceProduct(PriceProduct priceProduct) {
        PriceProductDto dto = new PriceProductDto();
        dto.productId = priceProduct.getProductId();
        dto.brandId = priceProduct.getBrand().getId();
        dto.priceList = priceProduct.getPriceList();
        dto.price = priceProduct.getPrice();
        dto.startDate = priceProduct.getStartDate();
        dto.endDate = priceProduct.getEndDate();
        
        return dto;
    }
}

