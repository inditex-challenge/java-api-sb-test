package com.inditexApi.ports.repository;

import com.inditexApi.domains.PriceProduct;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * This interface is added to avoid get tied the DB implementation in use cases.
 */
public interface PriceProductRepository {
    Optional<PriceProduct> findById(Long id);
    List<PriceProduct> findAll();
    PriceProduct findCustomByProductIdAndBrandIdAndApplicationDate(Long productId, LocalDateTime applicationDate, Integer brandId);
}