package com.inditexApi.domains;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name = "PRICE_PRODUCT")
public class PriceProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRAND_ID")
    private Brand brand; // Reference to the Brand entity
    
    @Column(name = "START_DATE")
    private LocalDateTime startDate;
    
    @Column(name = "END_DATE")
    private LocalDateTime endDate;
    
    @Column(name = "PRICE_LIST")
    private Integer priceList;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Column(name = "PRICE")
    private BigDecimal price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CURR_ID")
    private Currency curr;

    public PriceProduct() {};

    public PriceProduct(Long id, Brand brand, LocalDateTime startDate, LocalDateTime endDate, Integer priceList,
                        Long productId, Integer priority, BigDecimal price, Currency curr) {
        this.id = id;
        this.brand = brand;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceList = priceList;
        this.productId = productId;
        this.priority = priority;
        this.price = price;
        this.curr = curr;
    }

    // Setters are no required for this example

    public Long getId() {
        return id;
    }

    public Brand getBrand() {
        return brand;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public Integer getPriceList() {
        return priceList;
    }

    public Long getProductId() {
        return productId;
    }

    public Integer getPriority() {
        return priority;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Currency getCurr() {
        return curr;
    }

    public Boolean isEmpty(PriceProduct price) {
        if (price == null ) return true;

        return (
            price.brand == null &&
            price.productId == null &&
            price.startDate == null &&
            price.endDate== null
            // For this case the rest of properties are irrelevants
        );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        PriceProduct objectToCompare = (PriceProduct) obj;
        return Objects.equals(id, objectToCompare.id) &&
               Objects.equals(brand, objectToCompare.brand) &&
               Objects.equals(startDate, objectToCompare.startDate) &&
               Objects.equals(endDate, objectToCompare.endDate) &&
               Objects.equals(priceList, objectToCompare.priceList) &&
               Objects.equals(productId, objectToCompare.productId) &&
               Objects.equals(priority, objectToCompare.priority) &&
               Objects.equals(price, objectToCompare.price) &&
               Objects.equals(curr, objectToCompare.curr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, startDate, endDate, priceList, productId, priority, price, curr);
    }
}
