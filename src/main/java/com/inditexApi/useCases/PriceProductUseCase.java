package com.inditexApi.useCases;

import com.inditexApi.adapters.persistence.PriceProductPersistant;
import com.inditexApi.domains.PriceProduct;
import com.inditexApi.ports.repository.PriceProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class PriceProductUseCase {

    private final PriceProductRepository priceProductPersistant;

    @Autowired
    public PriceProductUseCase(PriceProductPersistant priceProductPersistant) {
        this.priceProductPersistant = priceProductPersistant;
    }

    public List<PriceProduct> findAll() {
        return priceProductPersistant.findAll();
    }

    public Optional<PriceProduct> findById(Long id) {
        return priceProductPersistant.findById(id);
    }

    public PriceProduct findByProductIdAndStartDateBetweenAndBrandId(Long productId, LocalDateTime applicationDate, Integer brandId) {
        return priceProductPersistant.findCustomByProductIdAndBrandIdAndApplicationDate(productId, applicationDate, brandId);
    }

    // alter data methods are not necesary in this example
}
